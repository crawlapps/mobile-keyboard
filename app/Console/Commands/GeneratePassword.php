<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Traits\PasswordTrait;
use Illuminate\Console\Command;

class GeneratePassword extends Command
{
    use PasswordTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate password and store it in google sheet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('id', '1')->first();
        $pwd = $this->addPassword($user->email);
        \DB::table('users')
            ->where('id', '1')
            ->update(['password' => $pwd]);
        \Log::info($user);
    }
}
