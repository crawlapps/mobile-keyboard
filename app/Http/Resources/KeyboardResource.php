<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KeyboardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'key_text_color' => $this->key_text_color,
            'background_alpha' => $this->background_alpha,
            'key_background_color' => $this->key_background_color,
            'file_font' => ($this->file_font) ? asset('storage/keyboard/' . $this->file_font) : "",
            'key_border_color' => $this->key_border_color,
            'file_background' => ($this->file_background) ? asset('storage/keyboard/' . $this->file_background) : "",
            'file_keyboard' => ($this->file_keyboard) ? asset('storage/keyboard/' . $this->file_keyboard) : "",
        ];
    }
}
