<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FontResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'file_image' => ($this->file_image) ? asset('storage/images/' . $this->file_image) : "",
            'file_font' => ($this->file_font) ? asset('storage/fontfiles/' . $this->file_font) : "",
        ];
    }
}
