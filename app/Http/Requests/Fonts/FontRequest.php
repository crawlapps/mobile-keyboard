<?php

namespace App\Http\Requests\Fonts;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use function Couchbase\defaultDecoder;
use Route;

class FontRequest extends FormRequest
{
    public static $rules = [
        'name' => 'required',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;
        switch (Route::CurrentRouteName()) {
            case 'fonts.store':
            {
                $rules['image'] = 'required|mimes:jpeg,bmp,png';
                $rules['fontfile'] = 'required';
                return $rules;
            }
            case 'fonts.update':
            {
                return $rules;
            }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'name.*' => 'Font Name is required...',
            'image.*' => 'Font image is required and must be jpg/jpeg/png format...',
            'fontfile.*' => 'Font file is required...'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
