<?php

namespace App\Http\Requests\Keyboard;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Route;

class KeyboardRequest extends FormRequest
{
    public static $rules = [
        'key_text_color' => 'required',
        'background_alpha' => 'required',
        'key_background_color' => 'required',
        'key_border_color' => 'required',
        'category_id' => 'required',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;
        switch (Route::CurrentRouteName()) {
            case 'keyboard.store':
            {
                $rules['file_font'] = 'required';
                $rules['file_background'] = 'required|mimes:jpeg,bmp,png';
                $rules['file_keyboard'] = 'required|mimes:jpeg,bmp,png';
                return $rules;
            }
            case 'keyboard.update':
            {
                return $rules;
            }
            default:
                break;
        }
    }
    public function messages()
    {
        return [

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
