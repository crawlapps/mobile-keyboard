<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;

class UserController extends Controller
{
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function getnewtoken(Request $request)
    {
        try {
//            dd(env('PASSPORT_CLIENT_ID'));

//            $refresh_token = $request->header('Authorization');
            $refresh_token = $request->refresh_token;

            if (empty($refresh_token)) {
                return response()->json(['message' => "Refresh token required..."], 422);
            }

            $user = User::where('refresh_token', $refresh_token)->first();
            if ($user) {
                // $user->name is know as password
                $data = $this->get_token($user->email, $user->name);

                $user->access_token = $data->access_token;
                $user->refresh_token = $data->refresh_token;
                $user->save();

                $data->access_token = $user->access_token;
                $data->refresh_token = $user->refresh_token;
                return response()->json(['data' => $data], 200);
            } else {
                return response()->json(['data' => "Device not found or token not match...!"], 422);
            }
        } catch
        (Exception $e) {
            return response()->json(['data' => "Device not found..."], 422);
        }

    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
//        dd(env('PASSPORT_CLIENT_ID'));
            $validator = Validator::make($request->all(), [
                'device_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['message' => "Device id required..."], 422);
            }
            $email = 'user' . date('Ymdhis') . '@crawlapps.com';

            $user = new User;

            $user->name = $request->device_id;
            $user->email = $email;
            $user->password = bcrypt($request->device_id);
            $user->save();

            $data = $this->get_token($email, $request->device_id);

            if (!empty($data->error)){
                return response()->json(['data' => "Token not Generated..."], 422);
            }
            $user->access_token = $data->access_token;
            $user->refresh_token = $data->refresh_token;
            $user->save();

            $data->access_token = $user->access_token;
            $data->refresh_token = $user->refresh_token;

            return response()->json(['data' => $data], 200);
        } catch
        (Exception $e) {
            return response()->json(['data' => "Device not Registered..."], 422);
        }
    }

    public function get_token($email, $password)
    {
        $data = [
            'grant_type' => 'password',
  //          'client_id' => 2,
//            'client_secret' => "ViTopXavPwSPwHDayqjlLXHxLNdnDof1jN4EtIuH",
            'client_id' => env('PASSPORT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_CLIENT_SECRET'),
            'username' => $email,
            'password' => $password,
        ];

//        dd($data);
        $response = Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($response);
//        dd($response);
        // Get the data from the response
        $data = json_decode($response->getContent());
//        dd($data);
        return $data;
    }
}
