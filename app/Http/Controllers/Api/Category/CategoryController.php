<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Exception;

class CategoryController extends Controller
{
    public function index()
    {
        try {
            $category = Category::select('id','name')->orderBy('created_at', 'DESC')->get();
            return response()->json(['data' => $category],200);
        } catch
        (Exception $e) {
            return response()->json(['data' => "Category not found..."] , 422);
        }
    }
}
