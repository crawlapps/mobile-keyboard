<?php

namespace App\Http\Controllers\Api\Keyboard;

use App\Http\Controllers\Controller;
use App\Http\Resources\KeyboardResource;
use Illuminate\Http\Request;
use App\Models\Keyboard;
use Exception;

class KeyboardController extends Controller
{
    public function index(Request $request)
    {
        try {
            $category_id = ($request->category_id) ? $request->category_id : "";
            if ($category_id) {
                $keyboard = Keyboard::where('category_id', $category_id)->orderBy('created_at', 'DESC')->get();
            } else {
                $keyboard = Keyboard::orderBy('created_at', 'DESC')->get();
            }
            $keyboard = KeyboardResource::collection($keyboard);
            return response()->json(['data' => $keyboard], 200);
        } catch
        (Exception $e) {
            return response()->json(['data' => "Keyboard not found..."], 422);
        }
    }
}
