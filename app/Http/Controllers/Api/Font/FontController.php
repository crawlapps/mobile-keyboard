<?php

namespace App\Http\Controllers\Api\Font;

use App\Http\Controllers\Controller;
use App\Http\Resources\FontResource;
use App\Models\Font;
use Exception;
use Illuminate\Http\Request;

class FontController extends Controller
{
    public function index(){

        try {
            $font = Font::select('id','name','file_image','file_font')->orderBy('created_at', 'DESC')->get();
            $font = FontResource::collection($font);
            return response()->json(['data' => $font], 200);
        } catch
        (Exception $e) {
            return response()->json(['data' => "Font not found..."], 422);
        }
    }
}
