<?php

namespace App\Http\Controllers\Fonts;

use App\Http\Controllers\Controller;
use App\Http\Requests\Fonts\FontRequest;
use Illuminate\Http\Request;
use App\Models\Font;
use App\Traits\ImageTrait;

class FontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // dd(config('filesystems.default'));
        $font = Font::get();
        return view('pages.fonts.index', compact('font'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $font = false;
        return view('pages.fonts.create', compact('font'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FontRequest $request)
    {
        try {

            $image = ImageTrait::makeImage($request->image, 'images/');
            $fontfile = ImageTrait::makeImage($request->fontfile, 'fontfiles/');

            $font = new Font;
            $font->name = $request->name;
            $font->file_image = $image;
            $font->file_font = $fontfile;

            $font->save();
            return redirect()->back()->with('success', 'Font Added successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Font not added...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Font $font)
    {
//        dd($font);
        return view('pages.fonts.create', compact('font'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FontRequest $request, Font $font)
    {
//        dd($font);
        try {
            if ($request->hasFile('image')) {
                $image = ImageTrait::makeImage($request->image, 'images/');
            } else {
                $image = $font->file_image;
            }
            if ($request->hasFile('fontfile')) {
                $fontfile = ImageTrait::makeImage($request->fontfile, 'fontfiles/');
            } else {
                $fontfile = $font->file_font;
            }

            $font->name = $request->name;
            $font->file_image = $image;
            $font->file_font = $fontfile;
            $font->save();
            return redirect()->back()->with('success', 'Font Updated Successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Font not Updated...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Font $font)
    {
        try {
            $font->delete();
            return response()->json(['data' => 'Font deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Font not Deleted...', 'status' => '0']);

//            $e->getMessage();
        }
    }
}
