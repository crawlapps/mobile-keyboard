<?php

namespace App\Http\Controllers\Keyboard;

use App\Http\Controllers\Controller;
use App\Models\Keyboard;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\Keyboard\KeyboardRequest;
use Exception;

class KeyboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyboard = Keyboard::get();
        return view('pages.keyboard.index', compact('keyboard'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $keyboard = false;
        $category = Category::get();
        return view('pages.keyboard.create', compact('keyboard'), compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KeyboardRequest $request)
    {
//        dd($request);
        try {
            $file_font_image = ImageTrait::makeImage($request->file_font, 'keyboard/');
            $file_background_image = ImageTrait::makeImage($request->file_background, 'keyboard/');
            $file_keyboard_image = ImageTrait::makeImage($request->file_keyboard, 'keyboard/');

            $keyboard = new Keyboard;
            $keyboard->key_text_color = "#".str_replace('#', '', $request->key_text_color);
            $keyboard->background_alpha = $request->background_alpha;
            $keyboard->key_background_color = "#".str_replace('#', '',$request->key_background_color);
            $keyboard->file_font = $file_font_image;
            $keyboard->key_border_color = "#".str_replace('#', '', $request->key_border_color);
            $keyboard->file_background = $file_background_image;
            $keyboard->file_keyboard = $file_keyboard_image;
            $keyboard->category_id = $request->category_id;

            $keyboard->save();
            return redirect()->back()->with('success', 'Keyboard Added successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Keyboard not added...');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyboard $keyboard)
    {
        $category = Category::get();
        return view('pages.keyboard.create', compact('keyboard'), compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KeyboardRequest $request, Keyboard $keyboard)
    {
        try {
            if ($request->hasFile('file_font')) {
                $file_font_image = ImageTrait::makeImage($request->file_font, 'keyboard/');

            } else {
                $file_font_image = $keyboard->file_font;
            }
            if ($request->hasFile('file_background')) {
                $file_background_image = ImageTrait::makeImage($request->file_background, 'keyboard/');
            } else {
                $file_background_image = $keyboard->file_background;
            }

            if ($request->hasFile('file_keyboard')) {
                $file_keyboard_image = ImageTrait::makeImage($request->file_keyboard, 'keyboard/');
            } else {
                $file_keyboard_image = $keyboard->file_keyboard;
            }

            $keyboard->key_text_color = "#".str_replace('#', '', $request->key_text_color);
            $keyboard->background_alpha = $request->background_alpha;
            $keyboard->key_background_color = "#".str_replace('#', '', $request->key_background_color);
            $keyboard->file_font = $file_font_image;
            $keyboard->key_border_color = "#".str_replace('#', '', $request->key_border_color);
            $keyboard->file_background = $file_background_image;
            $keyboard->file_keyboard = $file_keyboard_image;
            $keyboard->category_id = $request->category_id;

            $keyboard->save();
            return redirect()->back()->with('success', 'Keyboard Updated Successfully...');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Keyboard not Updated...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyboard $keyboard)
    {
        try {
            $keyboard->delete();
            return response()->json(['data' => 'Keyboard deleted successfully...', 'status' => '1']);
        } catch (Exception $e) {
            return response()->json(['data' => 'Keyboard not Deleted...', 'status' => '0']);
        }
    }
}
