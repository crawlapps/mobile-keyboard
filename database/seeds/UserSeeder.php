<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'info@crawlapps.com',
            'email_verified_at' => now(),
            'password' => bcrypt('#Crawl885Apps!'), // password
            'remember_token' => Str::random(10),
        ]) ;
    }
}
