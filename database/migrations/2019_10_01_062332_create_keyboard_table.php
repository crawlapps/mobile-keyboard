<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keyboard', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key_text_color')->nullable();
            $table->string('background_alpha')->nullable();
            $table->string('key_background_color')->nullable();
            $table->string('file_font')->nullable();
            $table->string('key_border_color')->nullable();
            $table->string('file_background')->nullable();
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keyboard');
    }
}
