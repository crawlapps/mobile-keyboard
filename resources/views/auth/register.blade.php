<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Keyboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href={{asset('distribution/vendor/bootstrap/css/bootstrap.min.css')}}>
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href={{asset('distribution/css/orionicons.css')}}>
    <!-- theme stylesheet-->
    <link rel="stylesheet" href={{asset('distribution/css/style.default.css')}} id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href={{asset('distribution/css/custom.css')}}>
    <!-- Favicon-->
    <link rel="shortcut icon" href={{asset('distribution/img/favicon.png?3')}}>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<div class="page-holder d-flex align-items-center">
    <div class="container">
        <div class="row align-items-center py-5">
            <div class="col-5 col-lg-7 mx-auto mb-5 mb-lg-0">
                <div class="pr-lg-5"><img src={{asset('distribution/img/illustration.svg')}} alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-5 px-lg-4">
                <h1 class="text-base text-primary text-uppercase mb-4">Keyboard</h1>
                <h2 class="mb-4">Welcome back!</h2>
                <h2>Register</h2>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif


                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ url('/postregister') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>

                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>

                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="pic">Profile pic</label>
                        <input type="file" class="form-control" id="pic" name="pic">
                    </div>
                    <div class="form-group">
                        <label for="name">Gender</label>
                        <input type="radio" class="form-control" id="g1" name="gender" value="m" selected>Male
                        <input type="radio" class="form-control" id="g2" name="gender" value="F">Female
                    </div>
                    <div class="form-group">
                        <label for="name">phone</label>
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>
                    <div class="form-group">
                        <label for="name">status</label>
                        <select>
                            <option value="" disabled>Select status:</option>
                            <option value="1">Active</option>
                            <option value="0">Deactive</option>
                            <option value="2">Suspand</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <p class="mt-5 mb-0 text-gray-400 text-center">Design by <a href="https://bootstrapious.com/admin-templates"
                                                                    class="external text-gray-400">Bootstrapious</a> &
            Your company</p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)                 -->
    </div>
</div>
<!-- JavaScript files-->
<script src={{asset('distribution/vendor/jquery/jquery.min.js')}}></script>
<script src={{asset('distribution/vendor/popper.js/umd/popper.min.js')}}></script>
<script src={{asset('distribution/vendor/bootstrap/js/bootstrap.min.js')}}></script>
<script src={{asset('distribution/vendor/jquery.cookie/jquery.cookie.js')}}></script>
<script src={{asset('distribution/vendor/chart.js/Chart.min.js')}}></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src={{asset('distribution/js/front.js')}}></script>
</body>
</html>
