@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-10">
                <h2>Keyboard</h2>
            </div>
            <div class="col-md-2">
                <a class="btn btn-lg form-control add-btn" href="{{ route('keyboard.create') }}">Create Keyboard</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Text Color</th>
                        <th>Background Alpha</th>
                        <th>Backgorund color</th>
                        <th>Font File</th>
                        <th>Keyboard Color</th>
                        <th>Background File</th>
                        <th>Keyboard File</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach( $keyboard as $key => $val )
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$val->key_text_color}}</td>
                            <td>{{$val->background_alpha}}</td>
                            <td>{{$val->key_background_color}}</td>
                            <td>{{$val->file_font}}</td>
                            <td>{{$val->key_border_color}}</td>
                            <td><img src="{{ asset('storage/keyboard/'. $val->file_background)}}" width="100"></td>
                            <td><img src="{{ asset('storage/keyboard/'. $val->file_keyboard)}}" width="100"></td>
                            <td>
                                <a href="{{route('keyboard.edit',$val->id)}}" class="fa fa-edit action"></a>
                                <a class="fa fa-trash del-keyboard" data-id="{{$val->id}}"></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif

    <script type="text/javascript">
        var crawlapps_keyboard = {
            init: function () {
                this.keyboard_delete();
            },
            keyboard_delete: function () {
                $(document).on('click', '.del-keyboard', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this Keyboard...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('keyboard.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
        };
        $(document).ready(function () {
            crawlapps_keyboard.init();
        });
    </script>
@endsection
