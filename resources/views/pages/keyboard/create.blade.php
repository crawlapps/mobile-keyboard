@extends('layouts.layout')
@section('content')
    <div class="container py-5" style="padding:10px;">

        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h6 text-uppercase mb-0">Save Keyboard</h3>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        <form method="post" action="{{($keyboard) ? route('keyboard.update',$keyboard->id) : route('keyboard.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if($keyboard)
                                @method('patch')
                            @endif
                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Text color</label>
                                <input type="text" placeholder="Text color" name="key_text_color" class="form-control" value="{{($keyboard) ? $keyboard->key_text_color : old('key_text_color')}}">
                                @if ($errors->has('key_text_color'))
                                    <span class="text-danger">{{ $errors->first('key_text_color') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Background Alpha</label>
                                <input type="text" placeholder="Background Alpha" name="background_alpha" class="form-control" value="{{($keyboard) ? $keyboard->background_alpha : old('background_alpha')}}">
                                @if ($errors->has('background_alpha'))
                                    <span class="text-danger">{{ $errors->first('background_alpha') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Background Color</label>
                                <input type="text" placeholder="Background Color" name="key_background_color" class="form-control" value="{{($keyboard) ? $keyboard->key_background_color : old('key_background_color')}}">
                                @if ($errors->has('key_background_color'))
                                    <span class="text-danger">{{ $errors->first('key_background_color') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Font File ( ttf/otf file)</label>
                                <input type="file" placeholder="Font file" name="file_font" class="form-control" value="{{($keyboard) ? $keyboard->file_font : ""}}">
                                @if ($errors->has('file_font'))
                                    <span class="text-danger">{{ $errors->first('file_font') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Border Color</label>
                                <input type="text" placeholder="Border Color" name="key_border_color" class="form-control" value="{{($keyboard) ? $keyboard->key_border_color : old('key_border_color')}}">
                                @if ($errors->has('key_border_color'))
                                    <span class="text-danger">{{ $errors->first('key_border_color') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Background File</label>
                                <input type="file" placeholder="Background File" name="file_background" class="form-control" value="{{($keyboard) ? $keyboard->file_background : ""}}">
                                @if ($errors->has('file_background'))
                                    <span class="text-danger">{{ $errors->first('file_background') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Keyboard File</label>
                                <input type="file" placeholder="Background File" name="file_keyboard" class="form-control" value="{{($keyboard) ? $keyboard->file_keyboard : ""}}">
                                @if ($errors->has('file_keyboard'))
                                    <span class="text-danger">{{ $errors->first('file_keyboard') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="form-control-label text-uppercase">Category</label>
                                <select class="form-control" name="category_id">
                                    <option value=""  disabled {{($keyboard) ? "" : "selected" }}> Select Category</option>
                                    @foreach($category as $ck=>$cv)
                                        <option value="{{$cv->id}}" {{ ($keyboard) ?  ($cv->id == $keyboard->category_id) ? "selected" : "" : ""}}>{{$cv->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('file_background'))
                                    <span class="text-danger">{{ $errors->first('file_background') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
