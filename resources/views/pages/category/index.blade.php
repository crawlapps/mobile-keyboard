@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row p-xl-5">
            <div class="col-md-10">
                <h2>Category</h2>
            </div>
            <div class="col-md-2">
             <a class="btn btn-lg form-control add-btn pull-right" href="{{ route('category.create') }}">Create Category</a>
            </div>
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach( $category as $key => $val )
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$val->name}}</td>
                            <td>
                                <a href="{{route('category.edit',$val->id)}}" class="fa fa-edit action"></a>
                                <a class="fa fa-trash del-category" data-id="{{$val->id}}"></a>

                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if(Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif
    <script type="text/javascript">
        var crawlapps_keyboard = {
            init: function () {
                this.category_delete();
            },
            category_delete: function () {
                $(document).on('click', '.del-category', function () {
                    var id = $(this).data('id');
                    swal({
                            title: "Are you sure?",
                            text: "To delete this category...",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                            showCancelButton: true,
                            cancelButtonText: 'Cancel',
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            let url = "{{route('category.destroy',"__")}}";
                            url = url.replace("__", id);
                            $.ajax({
                                url: url,
                                type: "post",
                                data: {'_method': 'delete', '_token': "{{ csrf_token() }}"},
                                success: function (data) {
                                    swal({
                                        icon: "success",
                                    });
                                    if (data['status'] == "1") {
                                        window.location.reload();
                                    }
                                },
                            });
                        });
                });
            },
        };
        $(document).ready(function () {
            crawlapps_keyboard.init();
        });
    </script>
@endsection
