<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('user/add-user', 'Api\User\UserController@register');
Route::post('user/getnewtoken', 'Api\User\UserController@getnewtoken');


Route::group(['middleware' => ['auth:api']], function () {
   	Route::get('category/getCategory', 'Api\Category\CategoryController@index');
    Route::get('keyboard/getKeyboardFromCategory', 'Api\Keyboard\KeyboardController@index');
    Route::get('font/getFont', 'Api\Font\FontController@index');
});

