<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get('/', function () {
    return view('auth.login');
});

Route::get('google/setauth', 'Google\GoogleController@setAuth')->name('setauth');
Route::get('google/addworksheet', 'Google\GoogleController@addWorkSheet')->name('addworksheet');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');


    /**
     * category routes
     */
    Route::group(['namespace' => 'Category'], function(){
        Route::resource('category', 'CategoryController');

    });

    /**
     * fonts routs
     */
    Route::group(['namespace' => 'Fonts'], function(){
       Route::resource('fonts', 'FontController');
    });

    /**
     * keyboard routs
     */
    Route::group(['namespace' => 'Keyboard'], function(){
        Route::resource('keyboard', 'KeyboardController');
    });
});
